import time
import os

import psycopg2

while True:
    try:
        con = psycopg2.connect(
            database=os.environ.get('POSTGRE_NAME'), user=os.environ.get('POSTGRE_USER'), 
            password=os.environ.get('POSTGRE_PASS'), host="db", port="5432")
    except Exception:
        print('Postgres not ready')
        time.sleep(1)
        continue
    print('Postgres ready')
    break



