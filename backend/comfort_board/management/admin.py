from django.contrib import admin
from management import models

@admin.register(models.Status)
class StatusAdmin(admin.ModelAdmin):
    fields = ('name', 'order',)
    list_display = ('name', 'order',)


@admin.register(models.Tasks)
class TasksAdmin(admin.ModelAdmin):
    list_display = ('name', 'status', 'estimate', 'spend_time')
    fields = ('name', 'description', 'status', 'spend_time', 'estimate', 'asigne', 'date_created')
    readonly_fields = ('spend_time', 'date_created',)
