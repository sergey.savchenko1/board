from django.db.models import F

from management import models

def calc_first_position(prev_order, new_order, qs):
    res = prev_order - new_order
    id_list = []
    for value, obj in enumerate(qs, 1):
        if value <= res:
            id_list.append(obj.id)
    qs.filter(id__in=id_list).update(order=F('order')+1)


def calc_last_position(prev_order, new_order, qs):
    res = new_order - prev_order
    res =  qs.count() - res
    id_list = []
    for value, obj in enumerate(qs, 1):
        if value >= res and obj.order > 0:
            id_list.append(obj.id)
    qs.filter(id__in=id_list).update(order=F('order')-1)

def calc_order_new_column(instance, new_order, prev_order, prev_status):
    qs =  models.Tasks.objects.exclude(id=instance.id)
    # update order value for all next cards in new column
    qs.filter(order__gte=new_order, status=instance.status).update(
        order=F('order')+1
    )
    # update order value for all next cards in prev column
    qs.filter(order__gte=prev_order, status=prev_status).update(
        order=F('order')-1
    )

def calc_intermediate_value(new_order, prev_order, qs_list, instance, addition=True):
    if addition:
        res = new_order - prev_order
    else:
        res = prev_order - new_order

    index = qs_list.index(instance)
    if addition:
        id_list = []
        # get obj next for update. Number of steps contains var 'res'
        for x in range(res):
            index +=1
            obj = qs_list[index]
            if obj.order:
                id_list.append(obj.id)
    else:
        id_list = []
        # get obj next for update. Number of steps contains var 'res'
        for x in range(res):
            index -=1
            obj = qs_list[index]
            id_list.append(obj.id)

    qs = models.Tasks.objects.filter(id__in=id_list)
    if addition:
        qs.update(order=F('order')-1)
    else:
        qs.update(order=F('order')+1)