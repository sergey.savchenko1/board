from django.db import transaction
from rest_framework import serializers

from management import models
from management import utils

class TaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Tasks
        fields = '__all__'
        read_only_fields = ('spend_time', 'date_created',)

    @transaction.atomic
    def update(self, instance, validated_data):
        prev_order = instance.order
        prev_status = instance.status
        order = validated_data.pop('order')
        super().update(instance, validated_data)
        status = validated_data.get('status')
        #move card = True: if card was move within one a column
        move_card = self.context.get('request').data.get('move_card')
        if not status:
            return instance
        #without changes
        if instance.order == order and move_card:
            return instance
        obj_statuses = models.Tasks.objects.filter(status=status).order_by('order')
        if not isinstance(order, int) or not obj_statuses:
            #count() will return on one value more
            instance.order = obj_statuses.count()
            instance.save()
            return instance
        # if new task have first position
        elif order == 0 and move_card:
            utils.calc_first_position(prev_order, order, obj_statuses)
        # if new task have last position
        elif order == obj_statuses.last().order and move_card:
            utils.calc_last_position(prev_order, order, obj_statuses)
        # this block code works when the card does not have the last or first position
        else:
            instance.order = order
            instance.save()
            l_obj_statuses = list(obj_statuses)
            if not move_card:
                utils.calc_order_new_column(instance, order, prev_order, prev_status)

            elif order > prev_order:
                utils.calc_intermediate_value(order, prev_order, l_obj_statuses, instance, addition=True)
            else:
                utils.calc_intermediate_value(order, prev_order, l_obj_statuses, instance, addition=False)
            return instance

        instance.order = order
        instance.save()
        return instance

    def create(self, validated_data):
        status = validated_data.get('status')
        # set default status, if him missing in req
        if not status:
            status = validated_data['status'] = models.Status.objects.first()
        obj_statuses = models.Tasks.objects.filter(status=status).order_by('order')
        # set default order, if him missing in req
        order = validated_data.get('order')
        if not isinstance(order, int):
            #count() is calculate from 1, but order calculate from 0
            #count() will return on one value more
            validated_data['order'] = obj_statuses.count()
            return super().create(validated_data)
        return super().create(validated_data)


class StatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Status
        fields = '__all__'