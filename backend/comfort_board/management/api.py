from datetime import datetime

from rest_framework import viewsets

from management import models
from management import serializers


class TaskViewSet(viewsets.ModelViewSet):
    queryset = models.Tasks.objects.all()
    serializer_class = serializers.TaskSerializer
    pagination_class = None

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.order_by('order')

    def get_serializer_context(self):
        return {'request': self.request}

    def update(self, request, *args, **kwargs):
        res = super().update(request, *args, **kwargs)
        obj = self.get_object()
        obj.order_date_chaned = datetime.now()
        if request.data.get('spend_time'):
            obj.spend_time += int(request.data.get('spend_time'))
        obj.save()
        return res

class StatusViewSet(viewsets.ModelViewSet):
    queryset = models.Status.objects.all()
    serializer_class = serializers.StatusSerializer
    http_method_names = ['get']