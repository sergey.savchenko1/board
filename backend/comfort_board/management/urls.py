from rest_framework import routers

from management import api

router = routers.DefaultRouter()
router.register(r'task', api.TaskViewSet)
router.register(r'status', api.StatusViewSet)
