from django.db import models

class Status(models.Model):

	name = models.CharField(max_length=15, unique=True)
	order = models.IntegerField(unique=True)

	class Meta:
		ordering = ('order',)
		verbose_name_plural = 'Status'

	def __str__(self):
		return self.name

class Tasks(models.Model):
	name = models.CharField(max_length=75)
	description = models.TextField()
	status = models.ForeignKey(Status, on_delete=models.SET_NULL, blank=True, null=True)
	estimate = models.DecimalField('Estimate time in Hours', max_digits=10, decimal_places=2, default=0)
	spend_time = models.DecimalField('Spent time in Hours', max_digits=10, decimal_places=2, blank=True, null=True, default=0)
	asigne = models.CharField(max_length=10, null=True, blank=True)
	date_created = models.DateTimeField(auto_now_add=True)
	order = models.PositiveIntegerField('Arrangement', blank=True)

	class Meta:
		ordering = ('date_created',)
		verbose_name_plural = 'Task'

	def __str__(self):
		return self.name