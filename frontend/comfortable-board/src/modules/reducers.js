const defaultState = {
	status: [],
    task: null,
    searchValue: null,
    createRoomPopup: false,
    boardView: 'board',
    taskPopup: false,
    showPreloader: true,
    currentTask: [],
    boardData: {lanes: []},
    taskList: []
}

function MainReducer(state=defaultState, action) {
    switch(action.type) {
        case 'STATUS':
            return {...state, status: action.value}
        case 'SEARCH_VALUE':
            return {...state, searchValue: action.value}
        case 'CREATE_ROOM_POPUP':
            return {...state, createRoomPopup: action.value}
        case 'BOARD_VIEW':
            return {...state, boardView: action.value}
        case 'TASK_POPUP':
            return {...state, taskPopup: action.value}
        case 'PRELOADER_STATUS':
            return {...state, showPreloader: action.value}
        case 'CURRENT_TASK':
            return {...state, currentTask: [action.value]}
        case 'BOARD_ACTION':
            return {...state, boardData: action.value}
        case 'TASK_LIST':
            return {...state, taskList: action.value}
        case 'ADD_TASK':
            return {...state, taskList: [...state.taskList, action.value]}
    }
 	return state;
}

export default MainReducer