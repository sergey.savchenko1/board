import React, { Component } from 'react';
import Board from 'react-trello'
import TopBarProgress from "react-topbar-progress-indicator";
import store from './modules/store';
import './app.css';
import './board-list-style.css';
import Header from './components/header/header.js';
import { DOMAIN } from './constants';
import { connect } from 'react-redux';

class App extends Component {

  componentDidMount() {
    this.getTask()
  }

  componentDidUpdate(prevProps) {
    if (this.props.listTasks != prevProps.listTasks) {
      this.hundlerTaks()
    } else if (this.props.searchValue != prevProps.searchValue) {
      this.hundlerTaks()
    }
  }

  sortCards = (a, b) => {
    //sort what will be the cards with the latest date.
    if (a.order > b.order) {
      return 1
    }
    if (a.order < b.order) {
      return -1
    }
    return 0
  }

  hundlerTaks = () => {
    let linesList = {lanes: []}
    let match = 0
    //iterate all statuses
    this.props.listStatuses.map((statusObj, i) => {
      let statusData = {
        id: statusObj.id.toString(),
        title: statusObj.name,
        cards: []
      }
      //iterate all task and find coincidence by status_id
      this.props.listTasks.map((taskObj, i) => {
        if (statusObj.id == taskObj.status) {
          if (this.props.searchValue && taskObj.name.search(this.props.searchValue) == -1) {
            return taskObj
          }
          match +=1
          statusData.cards.push(
            {id: taskObj.id.toString(), title: taskObj.name,
            description: taskObj.description,
            order: taskObj.order,
            label: taskObj.estimate, showDeleteButton: false}
          )
        }
        return taskObj
      })
      statusData.cards.sort(this.sortCards)
      statusData.label = match.toString()
      linesList.lanes.push(statusData)
      match = 0
      return statusObj
    })
    store.dispatch({type: 'PRELOADER_STATUS', value: false})
    store.dispatch({type: 'BOARD_ACTION', value: linesList})
  }

  async getTask() {
    await this.getStatus()
    fetch(DOMAIN + 'task/')
    .then(res => res.json())
    .then(json => {
      store.dispatch({type: 'TASK_LIST', value: json});
      this.hundlerTaks()
    })
  }

  getStatus = () => {
    return (
      fetch(DOMAIN + 'status/')
      .then(res => res.json())
      .then(json => {
        store.dispatch({type: 'STATUS', value: json});
        return
      })
    )
  }

  moveToColumn = (cardId, sourceLaneId, targetLaneId, position, cardDetails) => {
    let prevOrde = cardDetails['order']
    let moveCard = sourceLaneId === targetLaneId
    fetch(DOMAIN + `task/${cardId}/`, {method: 'PATCH',
                                      headers: {'Content-Type':'application/json'},
                                      body: JSON.stringify({'status': targetLaneId,
                                                            'order': position,
                                                            'move_card': moveCard})})
    .then((res) => {
      if (res.status === 200) {
        this.props.listTasks.map((obj) => {
          //change old status_id to new status_id for current task
          if (cardId == obj.id) {
            obj.status = targetLaneId
            obj.order = position
          }
          if (!moveCard) {
            //calculcate order for next items in  new column
            if (obj.status == targetLaneId && obj.id != cardId && obj.order >= position) {
              obj.order +=1
            //calculcate order for next items in  prev column
            } else if (obj.status == sourceLaneId && obj.order > prevOrde) {
              obj.order -=1
            }
          }
          return obj
        })
        if (!moveCard) {
          this.hundlerTaks()
        }
      }
    })
  }

  showTaskPopup = (laneId) => {
    store.dispatch({type: 'TASK_POPUP', 'value': true})
    fetch(DOMAIN + `task/${laneId}/`)
    .then(res => res.json())
    .then(json => {
      store.dispatch({type: 'CURRENT_TASK', value: json})
    })
  }

  changeTask = (event) => {
    if (event.target.id == 'title') {
      let obj = this.props.currentTask[0]
      obj.name = event.target.value
      store.dispatch({type: 'CURRENT_TASK', value: obj})
    } else if (event.target.id == 'desc') {
      let obj = this.props.currentTask[0]
      obj.description = event.target.value
      store.dispatch({type: 'CURRENT_TASK', value: obj})
    } else if (event.target.id == 'status') {
      let obj = this.props.currentTask[0]
      obj.status = event.target.value
      store.dispatch({type: 'CURRENT_TASK', value: obj})
    } else if (event.target.id == 'estimate') {
      let obj = this.props.currentTask[0]
      obj.estimate = event.target.value
      store.dispatch({type: 'CURRENT_TASK', value: obj})
    } else if (event.target.id == 'spend') {
      let obj = this.props.currentTask[0]
      obj.spendNew = event.target.value
      store.dispatch({type: 'CURRENT_TASK', value: obj})
    }
  }

  updateTask = () => {
    let taskObj = this.props.currentTask[0]
    fetch(DOMAIN + `task/${taskObj.id}/`, {method: 'PATCH',
                                          headers: {'Content-Type':'application/json'},
                                          body: JSON.stringify({
                                            'name': taskObj.name,
                                            'description': taskObj.description,
                                            'status': taskObj.status,
                                            'estimate': taskObj.estimate,
                                            'spend_time': taskObj.spendNew,
                                            'order': taskObj.order,
                                          })})
    .then(res => {
      if (res.ok) {
        this.props.listTasks.map((obj) => {
          if (taskObj.id == obj.id) {
            obj.name = taskObj.name
            obj.description = taskObj.description
            obj.status = taskObj.status
            obj.estimate = taskObj.estimate

            if (taskObj.spendNew) {
              taskObj.spend_time = parseFloat(obj.spend_time) + parseFloat(taskObj.spendNew)
              obj.spend_time = parseFloat(obj.spend_time) + parseFloat(taskObj.spendNew)
            }
            store.dispatch({type: 'CURRENT_TASK', value: taskObj})
          }
          return obj
        })
        this.hundlerTaks()
      }
    })
  }

  changeLayout = () => {
    if (this.props.boardView == 'board') {
      store.dispatch({type: 'BOARD_VIEW', value: 'list'})
    } else {
     store.dispatch({type: 'BOARD_VIEW', value: 'board'})
    }
  }

  render() {
    if (this.props.preloaderStatus) {
      return (<TopBarProgress />)
    } else {
      return (
        <div>
          {/*popup*/}
          <div className={this.props.taskPopup ? 'task-popup-show' : 'task-popup-hide'}>
            <p className='clsoe-button-popup' onClick={() => store.dispatch({type: 'TASK_POPUP', 'value': false})}>close</p>
              {this.props.currentTask.map((obj, i) => {
                return(
                  <div key={i} className='task-data'>
                    <p>TITLE</p>
                    <input id='title' className='popup-input' value={this.props.currentTask[0].name} onChange={(event) => this.changeTask(event)} />
                    <p>Description</p>
                    <textarea id='desc' className='popup-input popup-input-desc'  value={this.props.currentTask[0].description} onChange={(event) => this.changeTask(event)} />
                    <p>Estimate Time: <input className='popup-input input-estimate' id='estimate' value={this.props.currentTask[0].estimate} onChange={(event) => this.changeTask(event)} /> h</p>
                    <p>Spended Time: {obj.spend_time}h + <input id='spend' className='popup-input input-estimate' onChange={(event) => this.changeTask(event)} /> h</p>
                    <select id='status' value={obj.status} onChange={(event) => this.changeTask(event)}>
                      {this.props.listStatuses.map((obj, i) => {
                          return(<option key={i} value={obj.id}>{obj.name}</option>)
                      })}
                    </select>
                    <p className="popup-save-btn" onClick={() => this.updateTask()}>Save</p>
                  </div>
                )
              })}
          </div>
          <Header />
          <div className="wrap-sub-header">
            <p>Show Like: <span className={this.props.boardView == 'board' ? 'show-like' : 'show-like-selected'} onClick={() => {this.changeLayout()}}>List</span></p>
          </div>
          <Board
            className={this.props.boardView == 'board' ? '' : 'list-view'}
            data={this.props.boardData}
            handleDragEnd={(cardId, sourceLaneId, targetLaneId, position, cardDetails) => this.moveToColumn(cardId, sourceLaneId, targetLaneId, position, cardDetails)}
            onCardClick={(laneId) => {this.showTaskPopup(laneId)}}
          />
        </div>
      )
    }
  }
}

function mapStateToProps(state) {
  return {
    boardView: state.boardView,
    taskPopup: state.taskPopup,
    preloaderStatus: state.showPreloader,
    currentTask: state.currentTask,
    boardData: state.boardData,
    listStatuses: state.status,
    listTasks: state.taskList,
    searchValue: state.searchValue,
  };
}

export default connect(mapStateToProps)(App);
