import React, { Component } from 'react';

import store from '../../modules/store';
import './header.css';
import CreateButton from '../create-button/create-button.js';

class Header extends Component {

	searchTask = (event) => {
		store.dispatch({type: 'SEARCH_VALUE', value: event.target.value});
	}

	render() {
		return(
			<div className='header-wrapper'>
				<input className='search' placeholder="Search.." onChange={(event) => this.searchTask(event)} />
				<p className='title'>Comfortable Board</p>
				<CreateButton />
			</div>
		)
	}
}

export default Header
