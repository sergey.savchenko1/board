import React, { Component } from 'react';
import store from '../../modules/store';
import './create-button.css';
import '../../app.css';
import { DOMAIN } from '../../constants';
import { connect } from 'react-redux';

class CreateButton extends Component {

	constructor(props) {
		super(props)
		this.state = {nameErr: '', descErr: '', estErr: ''}
		this.status = this.props.listStatuses
		this.nameRef = React.createRef();
		this.descRef = React.createRef();
		this.estRef = React.createRef();
		this.statusRef = React.createRef();
	}

	createTask = () => {
		this.nameRef.current.value
		fetch(DOMAIN + `task/`, {'method': 'POST',
								headers: {'Content-Type':'application/json'},
								body: JSON.stringify({'name': this.nameRef.current.value,
													'description': this.descRef.current.value,
													'estimate': this.estRef.current.value,
													'status': this.statusRef.current.value})})
		.then(res => {
            if (!res.ok) {
                res.json().then(json => {
                    this.setState({
                        nameErr: json.name,
                        descErr: json.description,
                        estErr: json.estimate
                    })
                })
            } else {
                store.dispatch({type: 'CREATE_ROOM_POPUP', value: false})
                return res.json()
            }
        })
        .then(json => {
            store.dispatch({type: 'ADD_TASK', value: json})
        })
	}

	render() {
		return(
			<div>
				<div className={this.props.createRoomPopup ? 'task-popup-show' : 'task-popup-hide'}>
		            <p className='clsoe-button-popup' onClick={() => store.dispatch({type: 'CREATE_ROOM_POPUP', value: false})}>close</p>
					<div>
						<div className='task-data'>
		                    <p>TITLE</p>
		                    <p className='err'>{this.state.nameErr}</p>
		                    <input id='title' className='popup-input' ref={this.nameRef} placeholder="title" />
		                    <p>Description</p>
		                    <p className='err'>{this.state.descErr}</p>
		                    <textarea id='desc' className='popup-input popup-input-desc' ref={this.descRef} placeholder="description" />
		                    <p>Estimate Time: <input className='estimate-inpt' ref={this.estRef} /> h</p>
		                    <p className='err'>{this.state.estErr}</p>
		                    <select id='status' ref={this.statusRef}>
		                      {this.status.map((obj, i) => {
		                         return(<option key={i} value={obj.id}>{obj.name}</option>)
		                      })}
		                    </select>
		                    <p className="popup-save-btn" onClick={this.createTask}>Save</p>
		                 </div>
					</div>
				</div>
				<div className='create-button-wrapper'>
					<p className='create-button' onClick={() => store.dispatch({type: 'CREATE_ROOM_POPUP', value: true})}>Create Task</p>
				</div>
			</div>
		)
	}
}

function mapStateToProps(state) {
  return {
    listStatuses: state.status,
    createRoomPopup: state.createRoomPopup
  };
}

export default connect(mapStateToProps)(CreateButton);
